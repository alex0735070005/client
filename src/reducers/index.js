import {combineReducers} from 'redux';

import app from "./app";
import cart from "./cart";
import product from "./product";

export default combineReducers({
    app,
    cart,
    product
});